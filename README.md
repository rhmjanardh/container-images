# CentOS Automotive Container Images

This repostory contains all the required files to build linux container images
that are used in AutoSD OS images.

Images are push to `quay.io/centos-sig-automotive/...`.

## License

[MIT](./LICENSE)
